.SILENT:

.PHONY: completions

install: completions /usr/bin/flush

uninstall:
	rm /usr/bin/flush

completions:
	cp completions/* /usr/share/bash-completion/completions/

/usr/bin/flush:
	echo "Installing $@..."
	cp tools/flush $@


